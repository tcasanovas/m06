#! /bin/bash
# @tomascasanovas
# Fitxers de configuracio abans de res
rm -rf /etc/nsswitch.conf
rm -rf /etc/nslcd.conf
rm -rf /etc/ldap/ldap.conf
rm -rf /etc/pam.d/common-session
rm -rf /etc/security/pam_mount.conf.xml
cp /opt/docker/nsswitch.conf /etc/nsswitch.conf
cp /opt/docker/nslcd.conf /etc/nslcd.conf
cp /opt/docker/ldap.conf /etc/ldap/ldap.conf
cp /opt/docker/common-session /etc/pam.d/common-session
cp /opt/docker/pam_mount.conf.xml /etc/security/pam_mount.conf.xml
# SAMBA 
# Activar els serveis
/usr/sbin/smbd 
/usr/sbin/nmbd  

#PAM
for user in unix01 unix02 unix03 unix04 unix05
do
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | passwd $user
done

/usr/sbin/nscd
/usr/sbin/nslcd -d




