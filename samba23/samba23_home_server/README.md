# Practica 22 Home server (pam+ldap+samba)

## Comprovacions

```
root@samba:/opt/docker# getent passwd
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/run/ircd:/usr/sbin/nologin
_apt:x:42:65534::/nonexistent:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
Debian-exim:x:100:102::/var/spool/exim4:/usr/sbin/nologin
messagebus:x:101:103::/nonexistent:/usr/sbin/nologin
nslcd:x:102:104:nslcd name service LDAP connection daemon,,,:/run/nslcd:/usr/sbin/nologin
unix01:x:1000:1000::/home/unix01:/bin/bash
unix02:x:1001:1001::/home/unix02:/bin/bash
unix03:x:1002:1002::/home/unix03:/bin/bash
unix04:x:1003:1003::/home/unix04:/bin/bash
unix05:x:1004:1004::/home/unix05:/bin/bash
smbunix01:x:1005:1005::/home/smbunix01:/bin/bash
smbunix02:x:1006:1006::/home/smbunix02:/bin/bash
smbunix03:x:1007:1007::/home/smbunix03:/bin/bash
smbunix04:x:1008:1008::/home/smbunix04:/bin/bash
smbunix05:x:1009:1009::/home/smbunix05:/bin/bash
pau:*:5000:600:Pau Pou:/tmp/home/pau:
pere:*:5001:600:Pere Pou:/tmp/home/pere:
anna:*:5002:600:Anna Pou:/tmp/home/anna:
marta:*:5003:601:Marta Mas:/tmp/home/marta:
jordi:*:5004:601:Jordi Mas:/tmp/home/jordi:
admin:*:10:27:Administrador Sistema:/tmp/home/admin:
user01:*:7001:610:user01:/tmp/home/1asix/user01:
user02:*:7002:610:user02:/tmp/home/1asix/user02:
user02:*:7003:610:user03:/tmp/home/1asix/user03:
user03:*:7003:610:user03:/tmp/home/1asix/user03:
user04:*:7004:610:user04:/tmp/home/1asix/user04:
user05:*:7005:610:user05:/tmp/home/1asix/user05:
user06:*:7006:611:user06:/tmp/home/2asix/user06:
user07:*:7007:611:user07:/tmp/home/2asix/user07:
user08:*:7008:611:user08:/tmp/home/2asix/user08:
user09:*:7009:611:user09:/tmp/home/2asix/user09:
user10:*:7010:611:user10:/tmp/home/2asix/user10:
```
```
root@samba:/opt/docker# pdbedit -L
user03:7003:user03
user05:7005:user05
user04:7004:user04
smbunix01:1005:
smbunix03:1007:
user06:7006:user06
user08:7008:user08
smbunix05:1009:
smbunix02:1006:
smbunix04:1008:
user10:7010:user10
user07:7007:user07
user09:7009:user09
pau:5000:Pau Pou
anna:5002:Anna Pou
pere:5001:Pere Pou
admin:10:Administrador Sistema
jordi:5004:Jordi Mas
marta:5003:Marta Mas
user01:7001:user01
user02:7002:user02

```

```
root@pam:/opt/docker# smbclient -U smbunix01%smbunix01 //samba.edt.org/smbunix01
Try "help" to get a list of possible commands.
smb: \> exit

root@pam:/opt/docker# smbclient -U anna%anna //samba.edt.org/marta
Try "help" to get a list of possible commands.
smb: \> ls
  .                                   D        0  Sun Feb 11 00:00:00 2024
  ..                                  D        0  Thu Apr 11 18:59:52 2024
  .profile                            H      807  Sun Apr 23 21:23:06 2023
  .bashrc                             H     3526  Sun Apr 23 21:23:06 2023
  .bash_logout                        H      220  Sun Apr 23 21:23:06 2023

		19480400 blocks of size 1024. 7031832 blocks available


root@pam:/opt/docker# su -l anna 
Creating directory '/tmp/home/anna'.
reenter password for pam_mount:
(mount.c:68): Messages from underlying mount program:
(mount.c:72): mount error(13): Permission denied
(mount.c:72): Refer to the mount.cifs(8) manual page (e.g. man mount.cifs) and kernel log messages (dmesg)
(pam_mount.c:522): mount of //samba.edt.org/anna failed

$ ls
mytmp
$ pwd
/tmp/home/anna

```

