#! /bin/bash
# @tomascasanovas
# -----------------------------
# users unix
for user in unix01 unix02 unix03 unix04 unix05
do
	useradd -m -s /bin/bash $user
	echo -e "$user\n$user" | passwd $user
done

/usr/sbin/nscd
/usr/sbin/nslcd

# share public (estructura de directoris)
mkdir -p /var/lib/samba/public
chmod 777 /var/lib/samba/public
cp /opt/docker/* /var/lib/samba/public/.

# share privat
mkdir -p /var/lib/samba/privat
cp /etc/os-release /var/lib/samba/privat/.

# copiar la configuració 
cp /opt/docker/smb.conf /etc/samba/smb.conf

# Creació usuaris samba/unix

for user in smbunix01 smbunix02 smbunix03 smbunix04 smbunix05
do 
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | smbpasswd -a $user
done

# script que afegeix usuaris ldap/smbunix/unix a samba
chmod +x /opt/docker/test1.sh
bash /opt/docker/test1.sh


# activar dimonis samba
/usr/sbin/smbd
/usr/sbin/nmbd -F

sleep infinity
