# Pràctica 20 SAMBA SHARES

## 1. Partim de la imatge pam ldap
Afegim al dockerfile el següents paquets:
> - samba
> - samba-client
> - cifs-utils
## 2. Compartir recursos docs, man i public

Dins de smb.conf
```
[documentation]
   comment = Documentacio a exportar
   path = /usr/share/doc
   guest ok = yes
   browseable = yes
   read only = yes

[man pages]
   comment = Pagines de manual
   path = /usr/share/man
   public = yes
   browseable = yes
   writable = no

[public]
   comment = Share contingut public
   path = /var/lib/samba/public
   public = yes
   browseable = yes
   writable = yes
   printable = no
   guest ok = yes

```
## 3. Creació usuaris samba/unix
Dins del startup.sh

```
for user in smbunix01 smbunix02 smbunix03 smbunix04 smbunix05
do 
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | smbpasswd -a $user
done
```

## 4.  
