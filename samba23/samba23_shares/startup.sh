#! /bin/bash
# @tomascasanovas
# -----------------------------

# share public (estructura de directoris)
mkdir -p /var/lib/samba/public
chmod 777 /var/lib/samba/public
cp /opt/docker/* /var/lib/samba/public/.

# share privat
mkdir -p /var/lib/samba/privat
cp /etc/os-release /var/lib/samba/privat/.

# copiar la configuració 
cp /opt/docker/smb.conf /etc/samba/smb.conf

# Config pam ldap
rm -rf /etc/nsswitch.conf
rm -rf /etc/nslcd.conf
rm -rf /etc/ldap/ldap.conf
rm -rf /etc/pam.d/common-session
rm -rf /etc/security/pam_mount.conf.xml
cp /opt/docker/nsswitch.conf /etc/nsswitch.conf
cp /opt/docker/nslcd.conf /etc/nslcd.conf
cp /opt/docker/ldap.conf /etc/ldap/ldap.conf
cp /opt/docker/common-session /etc/pam.d/common-session
cp /opt/docker/pam_mount.conf.xml /etc/security/pam_mount.conf.xml



# Creació usuaris samba/unix

for user in smbunix01 smbunix02 smbunix03 smbunix04 smbunix05
do 
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | smbpasswd -a $user
done


# activar dimonis samba
/usr/sbin/nmbd
/usr/sbin/smbd
# activar dimonis
/usr/sbin/nscd
/usr/sbin/nslcd -d

