#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# @edt ASIX M06 Curs 2023-2024
# parse = analitzador sintactic
# -------------------------------
import argparse

# ArgumentParser = constructor
parser = argparse.ArgumentParser(\
  description="programa d'exemple d'arguments",\
  prog="02-arguments.py",\
  epilog="hasta luego lucas!")
parser.add_argument("-n","--nom",type=str,\
  help="nom usuari")
parser.add_argument("-e","--edat",type=int,\
  dest="userEdat", help="edat a processar",\
  metavar="edat")
#processa els arguments
args=parser.parse_args()

# mostar nom i edat


print(args)
print(args.userEdat,args.nom)
exit(0)
