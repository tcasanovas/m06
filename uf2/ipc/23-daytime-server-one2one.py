# /user/bin/python3
#-*- coding: utf-8-*-
# @ Abril 2024
# SERVIDOR DAYTIME
# ------------------------------
import sys,socket, argparse
from subprocess import Popen, PIPE
parser=argparse.ArgumentParser(description="""daytime server""")
parser.add_argument("-p", "--port", type=int, default=50001)
args=parser.parse_args()
HOST = ''
PORT = args.port

# CREEM SOCKET
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# ESCOLTA
s.bind((HOST,PORT)) 
s.listen(1)

# fa un bucle infinit que accepta la connexio, i aten al client (li mostra la data d'avui)
while True:
    conn, addr = s.accept() 
    print("Connected by", addr)
    command = [ "date" ]
    pipeData = Popen(command, stdout=PIPE)
    for line in pipeData.stdout:
        conn.send(line)
    conn.close()






