# /user/bin/python3
#-*- coding: utf-8-*-
# @ Abril 2024
# SERVIDOR DAYTIME
# ------------------------------
# PER FER UN CLIENT RAPIDAMENT
# docker run --rm -h python --name python -v $(pwd):/test -p 55001:55001 -it python /bin/bash
#   instal·lar nmap iproute2 ncat -> per utilitzar nc
# ------------------------------
import sys,socket, argparse, os, signal
from subprocess import Popen, PIPE

parser=argparse.ArgumentParser(description="""daytime server""")
parser.add_argument("-p", "--port", type=int, default=50001)
args=parser.parse_args()
HOST = ''
PORT = args.port

# variable global
llista_peers=[]


# funcions senyals
def myuser1(signum,frame):
    print("Signal handler called with signal:", signum)
    print(llista_peers)
    sys.exit(0)

def myuser2(signum,frame):
    print("Signal handler called with signal:", signum)
    print(len(llista_peers))
    sys.exit(0)

def myuser3(signum,frame):
    print("Signal handler called with signal:", signum)
    print(llista_peers,len(llista_peers))
    sys.exit(0)


# CREEM SOCKET
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)




# el dimoni es queda en execució per despres fer un fork del seu pare
# que es governara amb senyals (ja que el pare es mor)
pid=os.fork()
# quan el pid es diferent de 0 sera el pare
if pid !=0:
    #os.wait()
    print("Programa pare: ",os.getpid(), pid)
    print("Llançant el procés fill servidor")
    sys.exit(0)

print("Programa fill: ", os.getpid(), pid)

# proces fill
signal.signal(signal.SIGUSR1, myuser1) #10
signal.signal(signal.SIGUSR2, myuser2) #12
signal.signal(signal.SIGTERM, myuser3) #12



# ESCOLTA
s.bind((HOST,PORT)) 
s.listen(1)

# fa un bucle infinit que accepta la connexio, i aten al client (li mostra la data d'avui)
while True:
    conn, addr = s.accept() 
    print("Connected by", addr)
    llista_peers.append(addr)
    command = "ss -ltn"
    pipeData = Popen(command, shell=True,stdout=PIPE)
    for line in pipeData.stdout:
        conn.send(line)
    conn.close()






