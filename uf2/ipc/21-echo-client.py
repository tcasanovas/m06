# /user/bin/python3
#-*- coding: utf-8-*-
# @ Abril 2024
# SERVIDOR ECHO
# ------------------------------
import sys, socket
HOST = '' # no posar res equival a 0.0.0.0 (totes les interficies)
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #AF_INET -> on el host es una string representant un hostname en un domini d'internet ex 'daring.cwi.nl' o una ip
# TCP perque estem construint un servidor ECHO
#s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.connect((HOST, PORT)) # pk es una tupla
s.send(b'hello,world')
data=s.recv(1024)
s.close()
print("Received:", repr(data))
sys.exit(0)