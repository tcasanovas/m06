#! /usr/bin/python3
#-*- coding: utf-8-*-
# Tomàs Casanovas Masegosa
import sys,socket,argparse

parser = argparse.ArgumentParser(description="""ps Client Agent""")
parser.add_argument("server",type=str)
parser.add_argument("-p","--port",type=int, default=50001)
args=parser.parse_args()
HOST = args.server
PORT = args.port
MYEOF = bytes(chr(4), 'utf-8') # YATA

# -----------------------------

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.connect((HOST, PORT))

while True:
  command = input("$ ")
  if not command: break
  s.send(bytes(command, 'utf-8'))
  while True:
    data = s.recv(1024)
    # QUAN REBEM LA INFO DE YATA NO SABEM EN QUIN ORDRE ARRIBA, PER TANT HEM DE CONTROLAR
    # ELS ULTIMS 4 CARACTERS PER ASSEGURAR-NOS QUE ES YATA (TRANSPORTISTA MUDANÇA)
    if data[-1:] == MYEOF : # QUAN REBIS EL YATA 
      print(str(data[:-1]))
      break
    print(data)

s.close()
sys.exit()

# EN TOT DIALEG NECESITEM UN PROTOCOL (ES A DIR, UNA SERIE DE NORMES PER DICTAR LES NORMES DEL DIALEG
# EXEMPLE QUAN PARLEN DUES PERSONES, UN PARLA, L'ALTRE ESCOLTA, DESPRES AL INREVES)
