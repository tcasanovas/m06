# /user/bin/python3
#-*- coding: utf-8-*-
# @ Abril 2024
# signal-exemple.py
# ------------------------------
import sys, os, signal

def myhandler(signum, frame):
    print("Signal handler with signal: ", signum)
    print("no em dona la gana de plegar")
    sys.exit(0)

def nodeath(signum, frame):
    print("Signal handler with signal: ", signum)
    print("Tururut! more't tu")

# Assignar un handler el senyal
# quan s'envia sigusr1 se li envia la funcio myhandler (manipulador)
# handler = encarregat de gestionar alguna cosa
signal.signal(signal.SIGUSR1,myhandler) #SIGUSR1 --> 10
signal.signal(signal.SIGUSR2,nodeath) #SIGUSR2 --> 12
signal.signal(signal.SIGALRM,myhandler) #SIGALRM --> 14
signal.signal(signal.SIGTERM,signal.SIG_IGN) #SIGTERM --> 15
signal.signal(signal.SIGINT,signal.SIG_IGN) #SIGINT --> 2


signal.alarm(60)    # passats 60 segons ens envii una alarma
print(os.getpid())
while True: # bucle infinit
    pass # instruccio que no fa res
sys.exit(0)