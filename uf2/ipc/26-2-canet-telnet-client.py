# /usr/bin/python3
#-*- coding: utf-8-*-
#
# telnet-client.py [-p port] server
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------
# 26-telnet-client.py -p port -s server
# 26-telnetServer.py [-p port] [-d debug]
# Implementar un servidor i un client telnet. Client i server fan un diàleg.
# Cal un senyal de “yatà” Usem chr(4).
# Si s’indica debug el server genera per stdout la traça de cada connexió.
import sys,socket
from subprocess import Popen, PIPE
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-p", "--port", type=int, dest="port", metavar="port",
       help="port del server", required=True)
parser.add_argument("-s", "--server", type=str, required=True, metavar="server",
       help="ip/host del servidor")
args=parser.parse_args()
HOST = args.server
PORT = args.port
MYEOF = bytes(chr(4), 'utf-8') # YATA
# -------------------------------------------------------------
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.connect((HOST, PORT))
while True:
    command = input("~$ ")
    if not command: break    
    s.sendall(bytes(command, 'utf-8'))    
    while True:
        data = s.recv(1024)
        # QUAN REBEM LA INFO DE YATA NO SABEM EN QUIN ORDRE ARRIBA, PER TANT HEM DE CONTROLAR
        # ELS ULTIMS 4 CARACTERS PER ASSEGURAR-NOS QUE ES YATA (TRANSPORTISTA MUDANÇA)
        if data[-1:] == MYEOF: # QUAN REBIS EL YATA 
            print(str(data[:-1]))
            break
        print(str(data))
s.close()
sys.exit(0)

# EN TOT DIALEG NECESITEM UN PROTOCOL (ES A DIR, UNA SERIE DE NORMES PER DICTAR LES NORMES DEL DIALEG
# EXEMPLE QUAN PARLEN DUES PERSONES, UN PARLA, L'ALTRE ESCOLTA, DESPRES AL INREVES)
