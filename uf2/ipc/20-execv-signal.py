# /user/bin/python3
#-*- coding: utf-8-*-
# @ Abril 2024
# ------------------------------

import sys, os
print("Hola, començament del programa principal")
print("PID pare: ",os.getpid())

pid=os.fork()
if pid !=0:
    print("Programa pare: ",os.getpid(), pid)
    sys.exit(0)

# programa fill

os.execv("/usr/bin/python3", ["/usr/bin/python3", "16-signal.py", "70"])

# No s'executa mai

print("Hasta luego lucas!")
sys.exit(0)