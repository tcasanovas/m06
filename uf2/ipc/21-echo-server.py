# /user/bin/python3
#-*- coding: utf-8-*-
# @ Abril 2024
# SERVIDOR ECHO
# ------------------------------
import sys, socket
HOST = '' # no posar res equival a 0.0.0.0 (totes les interficies)
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #AF_INET -> on el host es una string representant un hostname en un domini d'internet ex 'daring.cwi.nl' o una ip
# TCP perque estem construint un servidor ECHO
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
# Ha d'escoltar el echo
s.bind((HOST,PORT)) # pk es una tupla
s.listen(1)
conn, addr = s.accept() # es queda clavat esperant a una connexió entrant (truca la porta)
# s.accept ens retorna una tupla
print("Conn:", type(conn), conn)
print("Connect by: ",addr)
while True:
    data = conn.recv(1024)
#if not data (no significa no hi han dades), vol dir si han tancat la connexió (si han penjat el telefon)
    if not data: break 
    conn.send(data)
    print(data)

conn.close()
sys.exit(0)