# 07 PAM pràctica: pam_time.so

Pràctica del mòdul pam_time.so
● Consulta el HowTo-PAM l’apartat “6 Exemples PAM: usant el servei account”
● Consulta el man de pam_time.so
● Consulta el man de /etc/security/time.conf

# Configura el container PAM per provar cada un dels casos següents:
```
root@pam:/etc/pam.d# cat chfn
account optional pam_echo.so [type:account rhost: %H lhost: %h service: %s terminal: %t ruser:%U user: %u]
account optional pam_echo.so [entra al time]
account sufficient pam_time.so
account optional pam_echo.so [surt del time]
account required pam_deny.so
```


● Tots els usuaris poden canviar el chfn en l’horari de 8-10

/etc/security/time.conf
```
chfn;*;*;Al0800-1000
```

● Cap usuari pot canviar el chfn en l’horari 8-10.
```
chfn;*;*;!Al0800-1000
```
● Tots els usuaris poden canviar el chfn en l’horari de 8-10 i de 16-22.
```
chfn;*;*;Al0800-1000 & Al1600-2200
```

● L’usuari unix01 pot canviar el finger de 8-14h. Els altres no.

```
chfn;*;unix01;Al0800-1400 → aquest no funciona del tot, no se si es per la comanda o per alguna altre clàusula dels arxius
```

● L'usuari pere només es pot connectar els dies entre setmana (working days)
```
login;*;pere;Wk0000-2400
```
● L’usuaria marta es pot connectar tots els dies per la tarda
```
login;*;marta;Al1200-2400
```
