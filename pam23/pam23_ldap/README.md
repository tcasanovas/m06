# DOCKER PAM LDAP

## 1 Comprovar que podem accedir a LDAP

```
root@pam:/opt/docker# nmap ldap.edt.org
Starting Nmap 7.93 ( https://nmap.org ) at 2024-01-15 11:42 UTC
Nmap scan report for ldap.edt.org (172.18.0.3)
Host is up (0.0000070s latency).
rDNS record for 172.18.0.3: ldap.edt.org.2hisx
Not shown: 998 closed tcp ports (reset)
PORT    STATE SERVICE
389/tcp open  ldap
636/tcp open  ldapssl
MAC Address: 02:42:AC:12:00:03 (Unknown)

Nmap done: 1 IP address (1 host up) scanned in 0.20 seconds

```
## 2 Editar fitxer /etc/ldap/ldap.conf
Canviar:
    -base
    -uri

## 3 Fem un ldapsearch per comprovar que carrega bé la base de dades
```
ldapsearch -x 
```
Ens ha de sortir la base de dades del nostre ldap (edt-org.ldif)

## 4 Permetre autentificació LDAP (fitxer nsswitch.conf)
Podem trobar aquest fitxer a /etc/nsswitch.conf
Afegir "ldap" a costat de files en el camps
    -passwd
    -group

```
# /etc/nsswitch.conf
#
# Example configuration of GNU Name Service Switch functionality.
# If you have the `glibc-doc-reference' and `info' packages installed, try:
# `info libc "Name Service Switch"' for information about this file.

passwd:         files ldap
group:          files ldap
shadow:         files
gshadow:        files

hosts:          files dns
networks:       files

protocols:      db files
services:       db files
ethers:         db files
rpc:            db files

netgroup:       nis

```
# 5 Configurar dimoni fitxer nslcd.conf

Podem trobar aquest fitxer a /etc/nslcd.conf
El contingut quedaria de la següent manera:
    -Canviar el base i el uri
    -base: dc=edt,dc=org
    -uri: ldap://ldap.edt.org
```
# /etc/nslcd.conf
# nslcd configuration file. See nslcd.conf(5)
# for details.

# The user and group nslcd should run as.
uid nslcd
gid nslcd

# The location at which the LDAP server(s) should be reachable.
uri ldap://ldap.edt.org 

# The search base that will be used for all queries.
base dc=edt,dc=org

# The LDAP protocol version to use.
#ldap_version 3

# The DN to bind with for normal lookups.
#binddn cn=annonymous,dc=example,dc=net
#bindpw secret

# The DN used for password modifications by root.
#rootpwmoddn cn=admin,dc=example,dc=com

# SSL options
#ssl off
#tls_reqcert never
tls_cacertfile /etc/ssl/certs/ca-certificates.crt

# The search scope.
#scope sub

```
## 6 Habilitar dimonis nscd i nslcd

```
/usr/sbin/nscd
/usr/sbin/nslcd
```
## 7 Comprovar que getent funciona amb un usuari ldap

```
root@pam:/opt/docker# getent passwd pere
pere:*:5001:600:Pere Pou:/tmp/home/pere:

```

## 8 Crear directori home automàticament si no existeix

Des de root, configurar /etc/pam.d/common-session
    -afegir la linia: session optional pam_mkdirhome.so
```
#
# /etc/pam.d/common-session - session-related modules common to all services
#
# This file is included from other service-specific PAM config files,
# and should contain a list of modules that define tasks to be performed
# at the start and end of interactive sessions.
#
# As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
# To take advantage of this, it is recommended that you configure any
# local modules either before or after the default block, and use
# pam-auth-update to manage selection of other modules.  See
# pam-auth-update(8) for details.

# here are the per-package modules (the "Primary" block)
session	[default=1]			pam_permit.so
# here's the fallback if no module succeeds
session	requisite			pam_deny.so
# prime the stack with a positive return value if there isn't one already;
# this avoids us returning an error just because nothing sets a success code
# since the modules above will each just jump around
session	required			pam_permit.so
# and here are more per-package modules (the "Additional" block)
session	required	pam_unix.so 
session optional	pam_mkhomedir.so
session	optional	pam_mount.so 
session	[success=ok default=ignore]	pam_ldap.so minimum_uid=1000
# end of pam-auth-update config

```
### 8.1 Comprovació
Per comprovar que ens crea directori home fem el següent:
```
root@pam:/opt/docker# su - unix01
unix01@pam:~$ su - pere
Password: 
Creating directory '/tmp/home/pere'.

```
## 9 Als usuaris LDAP se’ls ha de crear un recurs temporal, dins del home anomenat tmp. Un ramdisk de tipus tmpfs de 100M.
Anem el fitxer /etc/security/pam_mount.conf.xml

```
<volume
	user="*"
	fstype="tmpfs"
	options="size=100M,uid=%(USER),mode=0770"
	mountpoint="~%(USER)/home/tmp"
/>

```

### Container LDAP latest
```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -d tcasanovas/ldap23:latest
```

### Container PAM
```
docker run --rm --name pam.edt.org -h pam.edt.org --net 2hisx --privileged -d tcasanovas/pam23:ldap
```


