# 9.2 PAM autentificació LDAP escola

Configurar el container PAM (--privileged) per:

> - permetre l'autenticació local dels usuaris unix locals (unix01, unix02 i unix03).
> - permetre l'autenticació d'usuaris de LDAP de l'escola del treball.
> - als usuaris LDAP se'ls ha de crear el directori home automàticament si no existeix.
> - als usuaris se'ls munta automàticament dins el seu home un directori anomenat cloud corresponent al recurs WebDav de l'unitat de xarxa de l'escola de l'usuari.

Observacions de configuració:

> - URI ldap.escoladeltreball.org
> - BASE dc=escoladeltreball,dc=org
> - WevDav https://cloud.proxy.inf.edt.cat:5511/remote.php/dav/files/

## 1 En el startup.sh creem usuaris unix

- Tot seguit al entrar al container, primer entrar amb unix01 (per exemple) i entrar a unix02 des de unix01 per verificar l'autenticació local.

## 2 Configurem ldap.conf

Canviem les lineas de BASE i URI per poder contactar amb l'escola del treball

```
#
# LDAP Defaults
#

# See ldap.conf(5) for details
# This file should be world readable but not world writable.

BASE	dc=escoladeltreball,dc=org
URI	ldap://ldap.escoladeltreball.org

#SIZELIMIT	12
#TIMELIMIT	15
#DEREF		never

# TLS certificates (needed for GnuTLS)
TLS_CACERT	/etc/ssl/certs/ca-certificates.crt
```

## 3 Configurem nslcd.conf

Fitxer on estan els grups i users de la base de dades.

```
# The location at which the LDAP server(s) should be reachable.
uri ldap://ldap.escoladeltreball.org 

# The search base that will be used for all queries.
base dc=escoladeltreball,dc=org

```
## 4 Autenticació usuaris ldap de l'escola del treball

- Configurem fitxer nsswitch.conf, afegim "ldap" a passwd i a group 

```
# /etc/nsswitch.conf
#
# Example configuration of GNU Name Service Switch functionality.
# If you have the `glibc-doc-reference' and `info' packages installed, try:
# `info libc "Name Service Switch"' for information about this file.

passwd:         files ldap
group:          files ldap
shadow:         files
gshadow:        files

hosts:          files dns
networks:       files

protocols:      db files
services:       db files
ethers:         db files
rpc:            db files

netgroup:       nis

```

## 5 Als usuaris LDAP crear directori home si no existeix

- Configurar common-session
- Afegir: 
session optional	pam_mkhomedir.so

```
#
# /etc/pam.d/common-session - session-related modules common to all services
#
# This file is included from other service-specific PAM config files,
# and should contain a list of modules that define tasks to be performed
# at the start and end of interactive sessions.
#
# As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
# To take advantage of this, it is recommended that you configure any
# local modules either before or after the default block, and use
# pam-auth-update to manage selection of other modules.  See
# pam-auth-update(8) for details.

# here are the per-package modules (the "Primary" block)
session	[default=1]			pam_permit.so
# here's the fallback if no module succeeds
session	requisite			pam_deny.so
# prime the stack with a positive return value if there isn't one already;
# this avoids us returning an error just because nothing sets a success code
# since the modules above will each just jump around
session	required			pam_permit.so
# and here are more per-package modules (the "Additional" block)
session	required	pam_unix.so 
session optional	pam_mkhomedir.so
session	optional	pam_mount.so 
session	[success=ok default=ignore]	pam_ldap.so minimum_uid=1000
# end of pam-auth-update config

```

## 6 Montar a tots els usuaris dins del home un directori anomenat cloud corresponent al recurs WebDav

- Configurar pam_mount.conf.xml afegint el següent volum amb les caracteristiques corresponents.

```
<volume
	uid="*"
	fstype="davfs"
	path="https://cloud.proxy.inf.edt.cat:5511/remote.php/dav/files/%(USER)"
    mountpoint="~/cloud"
	options="username=%(USER),uid=%(USERUID),gid=%(USERGID),dir_mode=0770,file_mode=0770"
/>
```

## Desplegar containers

```
docker run --rm --name pam.edt.org -h pam.edt.org --net 2hisx --privileged -d tcasanovas/pam23:escola
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -d tcasanovas/ldap23:latest

```
