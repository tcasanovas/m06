# SSH BASE

## Inici
Crearem el pam "ssh23:base" a partir d'una copia de "ldap23:base"

### Dockerfile
Paquets necessaris:

nslcd, nslcd-utils, ldap-utils, libnss-ldapd, libpam-ldapd, davfs2, openssh-server openssh-client net-tools

```
# SSH
FROM debian:latest
LABEL author="Tomàs Casanovas"
LABEL subject="ssh"
RUN apt-get update
#ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -y install procps iproute2 tree nmap vim less finger passwd libpam-pwquality libpam-mount libnss-ldapd libpam-ldapd nslcd nslcd-utils ldap-utils davfs2 openssh-server openssh-client net-tools
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh

```

### Fitxers necessaris
**/etc/ldap/ldap.conf**: Fitxer que ens permetra realitzar la connexió sense utilitzar IP i base de dades amb el servidor ldap.
**Comprovació: ldapsearch -x UID='pere'**
```
BASE	dc=edt,dc=org
URI	ldap://ldap.edt.org
```

**/etc/nslcd.conf**: Anomena la BD on estan els users i grups.
```
# The location at which the LDAP server(s) should be reachable.
uri ldap://ldap.edt.org 

# The search base that will be used for all queries.
base dc=edt,dc=org

```
**/etc/nsswitch.conf**: Posem l'opció que els usuaris i grups també estan en ldap segint un ordre de prioritat.
**Comprovació: getent passwd "usuaris del ldap" pere**
```
passwd:         files ldap
group:          files ldap
```
**/etc/pam.d/common-session**: Fitxer on afegim l'opció que al iniciar sessió amb un usuari es creei el home.
Linea de pam_mkhomedir.so
```
# and here are more per-package modules (the "Additional" block)
session	required	pam_unix.so 
session optional	pam_mkhomedir.so
session	optional	pam_mount.so 
session	[success=ok default=ignore]	pam_ldap.so minimum_uid=1000
# end of pam-auth-update config
```

**/etc/security/pam_mount.conf.xml**:Fitxer on afegim l'opció de que qualsevol user se li creara un volum temporal de 100MB al inciar sessió.
```
		<!-- Volume definitions -->
<volume
	user="*"
	fstype="tmpfs"
	mountpoint="~/mytmp"
	options="size=100M"
	/>

<volume
	uid="5000-9999"
	fstype="davfs"
	path="http://dav.edt.org/webdav"
	mountpoint="~/apunts"
	options="username=%(USER),uid=%(USERUID),gid=%(USERGID),file_mode=0770,dir_mode=0770"
	/>

		<!-- pam_mount parameters: General tunables -->
```

### Edició startup.sh
Afegir servei ssh per a que es mantengi en FOREGROUND:
```
#! /bin/bash

for user in unix01 unix02 unix03 unix04 unix05
do 
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | passwd $user
done

/bin/bash

rm -rf /etc/nsswitch.conf
rm -rf /etc/nslcd.conf
rm -rf /etc/ldap/ldap.conf
rm -rf /etc/pam.d/common-session
rm -rf /etc/security/pam_mount.conf.xml
cp /opt/docker/nsswitch.conf /etc/nsswitch.conf
cp /opt/docker/nslcd.conf /etc/nslcd.conf
cp /opt/docker/ldap.conf /etc/ldap/ldap.conf
cp /opt/docker/common-session /etc/pam.d/common-session
cp /opt/docker/pam_mount.conf.xml /etc/security/pam_mount.conf.xml
# activar dimonis
/usr/sbin/nscd 
/usr/sbin/nslcd

mkdir /run/sshd
/usr/sbin/sshd -D

```

### Desplegar container 
Desplegar-ho manualment:
```
docker run --rm --name ssh.edt.org -h ssh.edt.org --net 2hisx -p 2022:22 --privileged -d tcasanovas/ssh23:base
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -d tcasanovas/ldap23:latest
```
### Desplegar amb compose.yml
Ho podem utilitzar el docker compose up -d i utilitzar el fitxer compose.yml que hem creat

