# Ldap23 editat

## Afegim usuaris i nova ou

- Dins del fitxer edt-org.ldif podem trobar el contigut del usuari0 fins usuaris11 i la ou bots

## Modificar RDN per uid

- En el meu cas creem fitxer modrdn.ldif i introduïm en la casella de newrdn: uid= el que sigui
- La comanda per que es modifiqui el RDN es la següent (no interactiva):
> - ldapmodify -x -D 'cn=Manager,dc=edt,dc=org' -w secret -f modrdn.ldif

## Canviar PASSWD

> - root@ldap:/opt/docker# slappasswd -h {MD5} -s secret
- {MD5}Xr4ilOzQ4PCOq3aQ0qbuaQ==

- Ara hem de copiar la contrasenya xifrada al slapd.conf

> - rootpw {MD5}Xr4ilOzQ4PCOq3aQ0qbuaQ==

## Iniciar container

> - docker run --rm --name ldap23editat -h ldap.edt.org --net 2hisx tcasanovas/ldap23:editat 
