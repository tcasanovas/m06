# LDAP SCHEMA PRÀCTICA

## Afegir schema al slapd.conf

```

vim slapd.conf
include 	/opt/docker/manga.schema

```

## Creació schema 

- Abaix de tot podem trobar el objecte STRUCTURAL (personatge) i el AUXILIAR (caracteristiques)
- junt amb tots els seus atributs
```
# manga.schema 
# 
# x-personatge:
# 	x-nom
# 	x-descripcio
# 	x-foto
# 	x-pdf
#
# x-caracteristiques:
# 	x-serie
#	x-alive
#	x-poder
#
#-------------------------------------

attributetype (1.1.2.1.1.1 NAME 'x-nom'
  DESC 'Nom del personatge'
  EQUALITY caseIgnoreMatch
  SUBSTR caseIgnoreSubstringMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE)

attributetype (1.1.2.1.1.2 NAME 'x-descripcio'
  DESC 'Descripció del personatge'
  EQUALITY caseIgnoreMatch
  SUBSTR caseIgnoreSubstringMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE)

attributetype (1.1.2.1.1.3 NAME 'x-foto'
  DESC 'Imatge del personatge'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.28)

attributetype (1.1.2.1.1.4 NAME 'x-pdf'
  DESC 'Pdf'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.5
  SINGLE-VALUE)

attributetype (1.1.2.2.1.1 NAME 'x-serie'
  DESC 'Serie a la que pertany el personatge'
  EQUALITY caseIgnoreMatch
  SUBSTR caseIgnoreSubstringMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE)

attributetype (1.1.2.2.1.2 NAME 'x-alive'
  DESC 'El personatge és viu o mort'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.7
  SINGLE-VALUE)

attributetype (1.1.2.1.1.1 NAME 'x-poder'
  DESC 'Indica el poder del personatge'
  EQUALITY caseIgnoreMatch
  SUBSTR caseIgnoreSubstringMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE)

#----------------------------------------

objectClass (1.1.2.1.1 NAME 'x-personatge'
  DESC 'Personatge'
  SUP TOP
  STRUCTURAL
  MUST x-nom
  MAY ( x-descripcio $ x-foto $ x-pdf ))

objectClass (1.1.2.2.1 NAME 'x-caracteristiques'
  DESC 'Caracteristiques del personatge'
  SUP TOP 
  AUXILIARY
  MUST ( x-serie $ x-alive )
  MAY x-poder)

```

## Creació OU 
- Dins de edt-org.ldif
```
dn: ou=practica,dc=edt,dc=org
objectClass: top
objectClass: organizationalunit
ou: practica
description: unitat organitzativa
```
## Creació 3 entitats

```
dn: x-nom=son goku,ou=practica,dc=edt,dc=org
objectClass: x-personatge
objectClass: x-caracteristiques
x-nom: son goku
x-descripcio: Protagonista principal de Bola de Drac
x-foto:< file:/opt/docker/goku.jpeg
x-pdf:< file:/opt/docker/goku.pdf
x-serie: Bola de Drac
x-alive: True
x-poder: Volar, gran velocitat, ones d'energia, força sobrehumana, etc.

dn: x-nom=shinnosuke nohara,ou=practica,dc=edt,dc=org
objectClass: x-personatge
objectClass: x-caracteristiques
x-nom: shinnosuke nohara
x-descripcio: Protagonista principal de Shin-Chan
x-foto:< file:/opt/docker/shin_chan.jpeg
x-pdf:< file:/opt/docker/shin_chan.pdf
x-serie: Shin-Chan
x-alive: True
x-poder: Extremadament graciós

dn: x-nom=doraemon,ou=practica,dc=edt,dc=org
objectClass: x-personatge
objectClass: x-caracteristiques
x-nom: doraemon
x-descripcio: Protagonista principal de Doraemon
x-foto:< file:/opt/docker/doraemon.jpeg
x-pdf:< file:/opt/docker/doraemon.pdf
x-serie: Doraemon
x-alive: True
x-poder: Butxaca màgica

```
## Encendre container
```
docker run --rm --name ldap -h ldap.edt.org --net 2hisx -p 389:389 -d tcasanovas/ldap23:practica

```
## Encendre phpldapadmin

```
docker run --rm  --name phpldapadmin.edt.org -h phpldapadmin.edt.org --net 2hisx -p 80:80 -d edtasixm06/ldap23:phpldapadmin 
```
