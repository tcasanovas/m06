# LDAP groups

## Enunciat

- Definir grups (gidNumber):
    - alumnes: 600
    - professors: 601
    - 1asix: 610
    - 2asix: 611
    - sudo: 27
    - 1wiam: 612
    - 2wiam: 613
    - 1hiaw: 614

## 1 Modificar RND perquè els usuaris s'identifiquin amb UID en comptes de CN

```
dn: uid=pau,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Pau Pou
cn: Pauet Pou
sn: Pou
homephone: 555-222-2220
mail: pau@edt.org
description: Watch out for this guy
ou: Profes
uid: pau
uidNumber: 5000
gidNumber: 600
homeDirectory: /tmp/home/pau
userPassword: {SSHA}NDkipesNQqTFDgGJfyraLz/csZAIlk2/

```
## 2 Afegir una OU anomenada grups

```
dn: ou=grups,dc=edt,dc=org
ou: grups
description: container per grups
objectClass: organizationalunit

```

## 3 Crear grups mencionats al enunciat

- Exemple:

```
dn: cn=alumnes,ou=grups,dc=edt,dc=org
objectClass: posixGroup
cn: alumnes
gidNumber: 600
description: Alumnes
memberUid: pau
memberUid: pere
memberUid: anna

```

- Assegurar el gidNumber dels usuaris a l'hora de assignar-lis el grup corresponent, per exemple:

```
dn: uid=user01,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user01
cn: alumne01 de 1asix
sn: alumne01
homephone: 555-222-0001
mail: user01@edt.org
description: alumne de 1asix
ou: 1asix
uid: user01
uidNumber: 7001
gidNumber: 610
homeDirectory: /tmp/home/1asix/user01
userPassword: {SHA}ovf8ta/reYP/u2zj0afpHt8yE1A=
```
## 4 Modificar user admin perquè sigui del grup 27 sudo

```
dn: uid=admin,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Administrador Sistema
cn: System Admin
sn: System
homephone: 555-222-2225
mail: anna@edt.org
description: Watch out for this girl
ou: system
ou: admin
uid: admin
uidNumber: 10
gidNumber: 27
homeDirectory: /tmp/home/admin
userPassword: {SSHA}4mS0FycWc5bkpW8/a396SGNDTQUlFSX3

```

## 5 Eliminar del slapd.conf els schemes que no necesitem, nomès els explícitament necessaris

```
include		/etc/ldap/schema/core.schema
include		/etc/ldap/schema/cosine.schema
include		/etc/ldap/schema/inetorgperson.schema
include		/etc/ldap/schema/misc.schema
include		/etc/ldap/schema/nis.schema
include		/etc/ldap/schema/openldap.schema

```
## Startup.sh perquè ens deixi entrar amb els protocols LDAP/LDAPs/LDAPi

```
#! /bin/bash


rm -rf /etc/ldap/slapd.d/*
rm -rf /var/lib/ldap/*
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
slapadd  -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
slapcat

chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap
cp /opt/docker/ldap.conf /etc/ldap/ldap.conf
/usr/sbin/slapd -d0 -h 'ldap:/// ldaps:/// ldapi:///'

```

## Comprovació

```
$ ldapsearch -xv -LLL -b 'ou=grups,dc=edt,dc=org'

ldap_initialize( <DEFAULT> )
filter: (objectclass=*)
requesting: All userApplication attributes
dn: ou=grups,dc=edt,dc=org
ou: grups
description: container per grups
objectClass: organizationalunit

dn: cn=sudo,ou=grups,dc=edt,dc=org
objectClass: posixGroup
cn: eltotpoderos
cn: sudo
gidNumber: 27
description: admin
memberUid: admin

........................
```

- Amb aquesta comanda ens llista tots els professors gràcies al gidNumber
```
$ ldapsearch -xv -LLL -b 'dc=edt,dc=org' 'gidNumber=601'

ldap_initialize( <DEFAULT> )
filter: gidNumber=601
requesting: All userApplication attributes
dn: cn=professors,ou=grups,dc=edt,dc=org
objectClass: posixGroup
cn: professors
gidNumber: 601
description: Professors
memberUid: marta
memberUid: jordi

dn: uid=marta,ou=usuaris,dc=edt,dc=org
objectClass: posixAccount
objectClass: inetOrgPerson
cn: Marta Mas
sn: Mas
homePhone: 555-222-2223
mail: marta@edt.org
description: Watch out for this girl
ou: Alumnes
uid: marta
uidNumber: 5003
gidNumber: 601
homeDirectory: /tmp/home/marta
userPassword:: e1NTSEF9OSsxRjJmNXZjVzh6L3RtU3pZTldkbHo1R2JEQ3lvT3c=
```
- Pels alumnes el mateix però canviant el gidNumber a 601
```
$ ldapsearch -xv -LLL -b 'dc=edt,dc=org' 'gidNumber=600'
```

## Comprovació connexions desde fora del container

- Ports 389 (LDAP) i 636 (LDAPs / LDAPi)
- Comprovació amb protocol ldap:
- Si no sabem quina IP posar, inspeccionem la xarxa en la qual estigui el container

```
$ ldapsearch -x -LLL -H ldap://172.19.0.2 -b 'dc=edt,dc=org' 'gidNumber=600'

dn: cn=alumnes,ou=grups,dc=edt,dc=org
objectClass: posixGroup
cn: alumnes
gidNumber: 600
description: Alumnes
memberUid: pau
memberUid: pere
memberUid: anna

```
- Comprovació amb protocol ldaps i ldapi

```

```

## Encendre container

> - $ docker run --rm --name ldap -h ldap.edt.org --net 2hisx -p 389:389 -p 636:636 tcasanovas/ldap23:grups


